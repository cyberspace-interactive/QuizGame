﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float animationDuration;

    public void LoadLevel(string sceneName)
    {
        StartCoroutine(LoadLevelCo(sceneName));
    }

    public IEnumerator LoadLevelCo(string sceneName)
    {
        transition.SetTrigger("StartTransition");

        yield return new WaitForSeconds(animationDuration);

        SceneManager.LoadScene(sceneName);
    }
}
