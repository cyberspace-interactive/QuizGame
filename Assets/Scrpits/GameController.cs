﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using TMPro;

public class GameController : MonoBehaviour
{
    public TextMeshProUGUI questionDisplayText;
    public TextMeshProUGUI scoreDisplayText;
    public TextMeshProUGUI timeRemainingDisplayText;
    public TextMeshProUGUI highScoreDisplay;
    
    public SimpleObjectPool answerButtonObjectPool;
    public Transform answerButtonParent;
    public GameObject questionDisplay;
    public GameObject levelEndDisplay;

    private LevelLoader levelLoader;
    private DataController dataController;
    private LevelData currentLevelData;
    private QuestionData[] questionPool;

    private int streakCounter;

    private bool isLevelActive;
    private float time;
    private IEnumerator timer;
    public IEnumerator answerButtonClicked;
    private float currentTimeToAnswer;
    private int questionIndex;
    private int playerScore;
    private List<GameObject> answerButtonGameObjects = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        dataController = FindObjectOfType<DataController>();
        levelLoader = FindObjectOfType<LevelLoader>();
        currentLevelData = dataController.GetCurrentLevelData();
        GetQuestions(dataController.language);
        //questionPool = currentLevelData.languages[0].questions;

        time = currentLevelData.time;
        timer = Timer(time);
        //UpdateTimeRemainingDisplay();

        streakCounter = 0;
        playerScore = 0;
        questionIndex = 0;

        ShuffleQuestions();

        ShowQuestion();
        StartTimer();
        isLevelActive = true;
    }

    private void GetQuestions(string language)
    {
        int languageIndex = 0;
        for(int i = 0; i < currentLevelData.languages.Length; i++)
        {
            if(currentLevelData.languages[i].langauge == language)
            {
                languageIndex = i;
            }
        }

        questionPool = currentLevelData.languages[languageIndex].questions;
    }

    private void ShuffleQuestions()
    {
        for (int i = 0; i < questionPool.Length; i++)
        {
            QuestionData temp = questionPool[i];
            int randomIndex = Random.Range(i, questionPool.Length);
            questionPool[i] = questionPool[randomIndex];
            questionPool[randomIndex] = temp;
        }
    }

    private void ShuffleAnswers(QuestionData questionData)
    {
        for (int i = 0; i < questionData.answers.Length; i++)
        {
            AnswerData temp = questionData.answers[i];
            int randomIndex = Random.Range(i, questionData.answers.Length);
            questionData.answers[i] = questionData.answers[randomIndex];
            questionData.answers[randomIndex] = temp;
        }
    }

    private void ShowQuestion()
    {
        RemoveAnswerButtons();
        QuestionData questionData = questionPool[questionIndex];

        ShuffleAnswers(questionData);

        questionDisplayText.text = questionData.questionText;

        for (int i = 0; i < questionData.answers.Length; i++)
        {
            GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();
            answerButtonGameObjects.Add(answerButtonGameObject);
            answerButtonGameObject.transform.SetParent(answerButtonParent, false);
            answerButtonGameObject.GetComponent<Image>().color = Color.white;

            AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
            answerButton.Setup(questionData.answers[i]);
            answerButton.answerButtonIndex = i;
        }
    }

    private void RemoveAnswerButtons()
    {
        while (answerButtonGameObjects.Count > 0)
        {
            answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
            answerButtonGameObjects.RemoveAt(0);
        }
    }

    private void ChangeAnswerButtonColor(Color color, int answerButtonIndex)
    {
        answerButtonGameObjects[answerButtonIndex].GetComponent<Image>().color = color;
    }

    public void AnswerButtonClicked(bool isCorrect, int answerButtonIndex)
    {
        answerButtonClicked = AnswerButtonClickedCo(isCorrect, answerButtonIndex);
        StartCoroutine(answerButtonClicked);
    }

    public IEnumerator AnswerButtonClickedCo(bool isCorrect, int answerButtonIndex)
    {
        if (isCorrect)
        {
            playerScore += currentLevelData.points;
            scoreDisplayText.text = playerScore.ToString();
            ChangeAnswerButtonColor(Color.green, answerButtonIndex);
            Streak(true);
        }       
        else
        {
            Streak(false);
            ChangeAnswerButtonColor(Color.red, answerButtonIndex);
            for (int i = 0; i < questionPool[questionIndex].answers.Length; i++)
            {
                if(questionPool[questionIndex].answers[i].isCorrect)
                {
                    ChangeAnswerButtonColor(Color.green, i);
                }
            }
        }

        yield return new WaitForSeconds(0.5f);

        if (questionPool.Length > questionIndex + 1)
        {
            questionIndex++;
            ShowQuestion();
            RestartTimer();
        }
        else
        {
            EndLevel();
        }
    }

    public void EndLevel()
    {
        isLevelActive = false;
        StopTimer();
        questionDisplay.SetActive(false);
        dataController.SubmitNewPlayerScore(playerScore);
        highScoreDisplay.text = "High Score: " + dataController.GetHighestPlayerScore().ToString();
        levelEndDisplay.SetActive(true);
    }

    public void ReturnToMenu()
    {
        levelLoader.LoadLevel("MainMenu");
    }

    public IEnumerator Timer(float time)
    {
        currentTimeToAnswer = time;
        while (currentTimeToAnswer > 0)
        {
            UpdateTimeRemainingDisplay();
            yield return new WaitForSeconds(1f);
            /*if (currentTimeToAnswer % 2 == 1)
            {
                currentPoints -= 10;
            }*/
            currentTimeToAnswer--;
        }
       
    }

    private void StartTimer()
    {
        StartCoroutine(timer);
    }

    private void RestartTimer()
    {
        StopCoroutine(timer);
        timer = Timer(time);
        StartCoroutine(timer);
    }

    private void StopTimer()
    {
        StopCoroutine(timer);
    }

    private void UpdateTimeRemainingDisplay()
    {
        timeRemainingDisplayText.text = Mathf.Round(currentTimeToAnswer).ToString();
    }

    private void Streak(bool isStreak)
    {
        if(isStreak)
        {
            streakCounter++;
        }
        else
        {
            streakCounter = 0;
        }
        Debug.Log(streakCounter);
    }

    void Update()
    {
        /*if (isLevelActive)
        {
            timeRemaining -= Time.deltaTime;
            UpdateTimeRemainingDisplay();

            if (timeRemaining <= 0f)
            {
                EndRound();
            }

        }*/

        if (currentTimeToAnswer <= 0)
        {
            if(questionPool.Length > questionIndex + 1)
            {
                questionIndex++;
                ShowQuestion();
                RestartTimer();
            }
            else
            {
                EndLevel();
            }
        }
    }
}