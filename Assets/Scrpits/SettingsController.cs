﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsController : MonoBehaviour
{
    private DataController dataController;
    private LevelLoader levelLoader;

    private GameObject currentActivePanel;

    public string currentLanguage;

    // Start is called before the first frame update
    void Start()
    {
        dataController = FindObjectOfType<DataController>();
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(currentActivePanel.activeSelf)
            {
                currentActivePanel.SetActive(false);
            }
            else
            {
                ReturnToMainMenu();
            }
        }
        
    }

    public void ReturnToMainMenu()
    {
        levelLoader.LoadLevel("MainMenu");
    }

    public void TurnPanelOn(GameObject panel)
    {
        currentActivePanel = panel;
        currentActivePanel.SetActive(true);
    }

    public void BackButton()
    {
        currentActivePanel.SetActive(false);
    }

    public void ChangeLanguage(string language)
    {
        dataController.language = language;
        BackButton();
    }
}
