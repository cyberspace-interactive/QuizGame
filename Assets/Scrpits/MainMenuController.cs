﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    private LevelLoader levelLoader;

    private void Start()
    {
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    public void LoadGame()
    {
        levelLoader.LoadLevel("Game");
    }

    public void LoadSettings()
    {
        levelLoader.LoadLevel("Settings");
    }
}
